# *Fit + Stark*
<br>
<br>
<br>
<br>
<br>
<br>
<br>
Sabine Hofmann    -    Jutta Steuer    -    Martin Kappel

---

### Inhalt

* Ausgangslage
* Von der Idee zum Schulfach
* Was ist Fit + Stark?
* Vorteile von Fit + Stark
* Übersicht über Module in Orientierungsstufe
* Ausblick auf Module der Klassen 7 - 9/10
* Arbeitsphase

---

### Ausgangslage

- Schwerpunkttag
  - 5 Mal im Jahr
  - 5.-10. Klasse
  - ganzer Vormittag
  - Methodentraining
  - nicht mehr zeitgemäß
  - losgelöst vom normalen Unterricht
  - Schüler und Lehrer wenig motiviert
- Methoden schwer in Unterricht integrierbar
- Doppelungen von Unterrichtsinhalten in verschiedenen Fächern

---

### Fit + Stark - Von der Idee zum Schulfach
Schuljahr 2015/2016
- Idee Fit + Stark einzuführen
- Besuch IGS Landau (Fach dort bereits eingeführt)
- Vorstellung von und Abstimmung über Fit + Stark im Kollegium
- Vorstellung von Fit + Stark im Schulausschuss
- Vorstellung von Fit + Stark im Schulelternbeirat

+++

<br>
<br>
- Abstimmung über Einführung von Fit + Stark in Gesamtkonferenz
- Planung, welche Fächer aus Stundentafel Stunden abtreten (im Kollegium)
- Einverständnis von Schulaufsicht für Einführung von Fit + Stark
- Planung der ersten Module für Stufe 5 und 6

---

### Fit + Stark - Von der Idee zum Schulfach

- Aug. 2016 Start von Fit + Stark in den Stufen 5 und 6
- Aug. 2017 Start von Fit + Stark in der Stufe 7

---

<br>
<br>
### **Fit** *für die Zukunft +*
### **stark** *durch Kompetenzen*

---

### Was ist Fit + Stark?

- Kompetenzorientierung
  - z.B. Lesekompetenz
  - Selbstorganisation
  - Methodenkompetenz
  - Sozialkompetenzen (Klassenrat)

+++

<br>
<br>
- Vorbereitung für das Leben nach der Schule
  - Selbstständigkeit
  - Teamfähigeit
  - …
  - Berufsorientierung
  - Berufswahl
  - Bewerbertraining

---

### Vorteile von Fit + Stark

- regelmäßig (2 Stunden/Woche)
- engere Verknüpfung mit Unterrichtsinhalten
- dynamischer als Schwerpunkttage
- zielorientierter auf Kompetenzdefizite der Schüler zugeschnitten
- kontinuierliche Arbeit
- Arbeit in Modulen (→ Zertifikate über erworbene Kompetenzen)
- Module bauen aufeinander auf
- Schülerorientierter + Lebensvorbereitender
- Inhalte für Berufswahlordner/Portfolio

---

### Übersicht über Module in Orientierungsstufe

- Textknacker / Lesetechnik
- Klassenrat
- Internet Recherche
- Berufswahlportfolio
- Präsentation (Kurzreferat)

+++

<br>
<br>
- Partnerarbeit
- Gruppenarbeit
- Mind-Mapping
- Effektiv Lernen 1
- Medienkompass

---

### Ausblick auf Module der Klassen 7-9/10

- Vorbereitung Profil AC (Kompetenzen, …)
- Berufswahl
- Bewerbertraining

---

### Arbeitsphase ( 😏 )

  *Haben Sie Ideen für neue Module bzw. Verbesserungsvorschläge?*
  *Was sollte jede Schülerin/jeder Schüler für sein Schulleben und die Zeit danach können und wissen?*

---

<br>
<br>
## Vielen Dank für Ihre Aufmerksamkeit! ☺️

---
